# egain-config-service
A HydraExpress microservice to interface with eGain products.

## Usage
Start the service with `node .`

To test, run `hydra-cli rest egain-config-service:[post]/agents/41377/modify /temp/params.json`
Or run `hydra-cli rest egain-config-service:[patch]/agents/41377 /temp/params.json`

### Example params.json
```json
{
  "attribute": "LAST_NAME",
  "value": "Smith"
}
```
