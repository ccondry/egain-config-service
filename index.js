const hydraExpress = require('hydra-express')
const env = require('node-env-file')
const pkg = require('./package.json')
const util = require('util')
const Egain = require('egain-config')

// load environment file
env('./.env')

// create eGain interface object
const egain = new Egain({
  fqdn: process.env.egain_fqdn,
  username: process.env.egain_username,
  password: process.env.egain_password
})

// set up hydra
const hydra = hydraExpress.getHydra()

// set up hydra and redis config
const hydraConfig = {
  hydra: {
    serviceName: pkg.name,
    serviceIP: process.env.hydra_service_ip || '',
    servicePort: process.env.hydra_service_port || 0,
    serviceType: process.env.hydra_service_type || '',
    serviceDescription: pkg.description,
    redis: {
      url: process.env.redis_url,
      port: process.env.redis_port,
      db: process.env.redis_db
    }
  }
}

const modifyAgent = async function(req, res) {
  console.log(req)
  // example body
  // req.body = {
  //   attribute: 'LAST_NAME',
  //   value: 'Smith'
  // }
  const skillTargetId = req.params.agent
  const attribute = req.body.attribute
  const value = req.body.value
  console.log(`request received to set ${attribute} of agent ${skillTargetId} to ${value}`)
  try {
    const data = await egain.agent.changeAttribute(skillTargetId, attribute, value)
    await egain.agent.updateScreenName(skillTargetId)
    console.log(`successfully modified agent ${skillTargetId}`)
    return res.status(200).send({data})
  } catch (error) {
    // ... error checks
    console.log(error)
    if (error.status) {
      return res.status(error.status).send(error)
    } else {
      return res.status(500).send({error})
    }
  }
}

// define routes
function onRegisterRoutes() {
  var express = hydraExpress.getExpress()
  var api = express.Router()

  api.post('/agents/:agent/modify', modifyAgent)
  api.patch('/agents/:agent', modifyAgent)

  hydraExpress.registerRoutes({
    '': api
  })
}

// start up
hydraExpress.init(hydraConfig, onRegisterRoutes)
.then(serviceInfo => {
  console.log('serviceInfo', serviceInfo)
  // connect the egain database
  egain.connect()
  // return 0
})
.catch(err => console.log('err', err))
