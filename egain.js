const env = require('node-env-file')
const pkg = require('./package.json')
const util = require('util')
const Egain = require('egain-config')

// load environment file
env('./.env')

// create eGain interface object
const egain = new Egain({
  fqdn: process.env.egain_fqdn,
  username: process.env.egain_username,
  password: process.env.egain_password
}).connect()

module.exports = egain
